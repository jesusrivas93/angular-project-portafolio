import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
declare var $:any;

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

	@Input()anchura:number;
	@Input()captions:boolean;
	@Output()getAutor = new EventEmitter();

	public autor:any;

  constructor() {
  	this.anchura=0;
  	this.captions=false;
  	this.autor = {
  		nombre:"Jesus Rivas",
  		website:"jesusrivas.es",
  		youtube:"jesusrivas93"
  	};
   }

  ngOnInit(): void {
  	$("#logo").click(function(e:any){
  		e.preventDefault();
  		$("header").css("background","green")
  		.css("height","50px");
  	});

  	$('.bxslider').bxSlider({
  		mode:'fade',
  		captions:this.captions,
  		slideWidth:this.anchura
  	});
  }

  launch(event:any){
  	console.log(event);
  	this.getAutor.emit(this.autor);
  }

}
