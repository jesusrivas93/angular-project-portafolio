import { Component, OnInit, ViewChild } from '@angular/core';
declare var $:any;

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

	public widthSlider:number;
	public anchuraToSlider:any;
	public captions:boolean;
	public autor:any;
	@ViewChild('textos',{static:true})textos:any;

  constructor() {
  	this.widthSlider=0;
  	this.anchuraToSlider=0;
  	this.captions=true;
   }

  ngOnInit(): void {
  	var opcion_clasica = document.querySelector('#texto')!.innerHTML;
  	console.log(this.textos.nativeElement.textContent);
  }

  cargarSlider(){
  	this.anchuraToSlider = this.widthSlider;
  }

  resetSlider(){
  	this.anchuraToSlider=false;
  }

  getAutorFromChild(event:any){
  	console.log(event);
  	this.autor=event;
  }  	

}
